# Infrastructure

Ansible deployment containing manifests for deploying a k8s-cluster

## Requirements

### Cluster nodes

* All nodes installed with Ubuntu 18.04.
* SSH access enabled
* [Other requirements](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#before-you-begin)

### SSH access

On the controller node, generate SSH keys:

```ssh-keygen -t rsa```

Copy the public key to the nodes:

```ssh-copy-id sigsoa@<node>```

### Controller node

* Updated host list in `/etc/hosts` in accordance with the inventory (e.g. kub[1-5] should be listed with an appropriate IP)
* Ansible 2.9
* Helm v.3
* [Docker collection](https://galaxy.ansible.com/community/docker) for Ansible
* [Kubernetes collection v.1.1.1](https://galaxy.ansible.com/community/kubernetes?extIdCarryOver=true&sc_cid=701f2000001OH6uAAG) for Ansible
* [Posix collection](https://galaxy.ansible.com/ansible/posix) for Ansible (used for mounting NFS volume)

## Installation

### Deploy k8s

```
ansible-playbook site.yml -i hosts -K
``` 

### Deploy NFS

```
ansible-playbook nfs.yml -i hosts -K 
```

### Deploy distributed block storage among the worker nodes (Longhorn)

```
ansible-playbook dist-block-storage.yml -i hosts -K
```

### Rollback

```
ansible-playbook rollback.yml -i hosts -K
```
