#!/bin/bash

# Add the user 'provision' to be used with Ansible
useradd -m -s /bin/bash provision

# Give the user a password
passwd provision

# Add user to sudo
echo -e 'provision\tALL=(ALL)\NOPASSWD:\tALL' > /etc/sudoers.d/provision

# Define user and SSH key
mkpasswd --method=SHA-512

# Switch to user
su - provision

# Generate key
ssh-keygen -t rsa -b 4096

for i in $(cat host-list.txt)
do
ssh-keyscan $i >> ~/.ssh/known_hosts
done
