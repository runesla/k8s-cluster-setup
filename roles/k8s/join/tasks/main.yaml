---
- name: Create join command
  command: kubeadm token create --print-join-command
  register: join_command
  delegate_to: "{{ groups['masters'][0] }}"

- name: Ensure node has not joined yet
  become: no
  community.kubernetes.k8s_info:
    kind: Node
    name: "{{ hostvars[inventory_hostname]['ansible_hostname'] }}"
  register: k8s_node
  delegate_to: "{{ groups['masters'][0] }}"

- name: Join cluster
  block:
  - name: Run kubeadm join (async) 
    command: "{{ join_command.stdout_lines[0] }}"
    async: 300
    poll: 0
    register: k8s_join
    when: 
    - k8s_node.resources == []
  - name: Check if the node joined
    become: no
    community.kubernetes.k8s_info:
      kind: Node
      name: "{{ ansible_hostname }}"
    delegate_to: "{{ groups['masters'][0] }}"
    register: cluster_node
    until: cluster_node.resources != []
    retries: 60
    delay: 5
  - name: Set the number of attempts of each host
    set_fact:
      num_attempts: "{{ num_attempts | default([]) + [hostvars[item]['cluster_node']['attempts']] }}"
    with_items: "{{ groups['workers'] }}"
  - name: Show the max amount of attempts
    debug:
      msg: "{{ num_attempts | max }}"
  - name: Show available timeout
    debug:
      msg: "{{ 300 - ((num_attempts | max) - 1) * 5 }}"
  - name: Check if node is ready
    become: no
    community.kubernetes.k8s_info:
      kind: Node
      name: "{{ ansible_hostname }}"
      wait: yes
      wait_timeout: "{{ 300 - ((num_attempts | max) - 1) * 5 }}"
      wait_condition:
        status: "True"
        type: "Ready"
    delegate_to: "{{ groups['masters'][0] }}"
  always:
  - name: Get cluster nodes
    become: no
    community.kubernetes.k8s_info:
      kind: Node
      name: "{{ ansible_hostname }}"
    register: cluster_node
    delegate_to: "{{ groups['masters'][0] }}"
  - name: Display cluster nodes
    debug:
      msg: "{{ cluster_node }}"
    delegate_to: "{{ groups['masters'][0] }}"